use cgmath::prelude::*;
use cgmath::{Rad, Point3, Quaternion};

use crate::constants::*;

// TODO: Implement kepler orbits

pub struct Orbit {
    radius: f64, // In meters
    inclination: Rad<f64>, // In radians
    longitude_of_asceding_node: Rad<f64>, // In radians
    period: f64, // In seconds
}

impl Orbit {
    pub fn get_pos(&self, t: f64) -> Point3<f64> {
        let point = Point3::new(self.radius, 0.0, 0.0);
        let phased_point = Quaternion::<f64>::from_angle_y(Rad(t / self.period * 2.0 * PI)).rotate_point(point);
        let inclined_point = Quaternion::<f64>::from_angle_x(self.inclination).rotate_point(phased_point);
        Quaternion::<f64>::from_angle_y(self.longitude_of_asceding_node).rotate_point(inclined_point)
    }

    pub fn get_bounding_size(&self) -> f64 {
        self.radius
    }
}

