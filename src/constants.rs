
pub const NANO: f64 = 1e-9;
pub const MICRO: f64 = 1e-6;
pub const MILLI: f64 = 1e-3;
pub const KILO: f64 = 1e3;
pub const MEGA: f64 = 1e6;
pub const GIGA: f64 = 1e9;

pub const LIGHT_YEAR: f64 = 9_460_730_472_580_800.0;
pub const PARSEC: f64 = 149_597_870_700.0;

pub const PI: f64 = std::f64::consts::PI;

